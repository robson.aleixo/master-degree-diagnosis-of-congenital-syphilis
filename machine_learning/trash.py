df_sinan_congenita = df_sinan_congenita[['ID_SINAN_SYPHILIS_CONGENITAL',
                                         'ID_SINAN_SYPHILIS', 'id_dataset_sinan',
                                         'syphilis_diagnosis', 'dt_notific', 'nu_ano', 'dt_diag', 'nu_idade',
                                         'female', 'id_syphilis_type', 'ant_idade', 'cs_sexo', 'evo_diag_n',
                                         'nu_notific', 'sem_not', 'id_municip', 'id_unidade', 'sg_uf_not',
                                         'sem_pri', 'cs_raca', 'cs_escol_n', 'evolucao', 'ant_pre_na',
                                         'ant_tratad', 'lab_parto', 'lab_titu_2', 'lab_dt3', 'lab_conf',
                                         'tra_esquem', 'ant_uf_cri', 'ant_muni_c', 'ant_local_', 'cli_assint',
                                         'cli_icteri', 'cli_rinite', 'cli_anemia', 'cli_esplen', 'cli_osteo',
                                         'cli_pseudo', 'cli_outro', 'labc_titul', 'labc_sangu', 'labc_igg',
                                         'labc_liquo', 'labc_evide', 'labc_liq_1', 'tra_diag_t', 'tra_esqu_1',
                                         'tp_not', 'id_agravo', 'dt_digita', 'dt_transse', 'ant_raca',
                                         'escolmae', 'antsifil_n', 'sem_diag']]

for i in list(df_sinan_congenita.columns):
    if df_sinan_congenita[i].isna().sum() > (0.3 * df_sinan_congenita.shape[0]):
        print(i)
        df_sinan_congenita.drop(columns=[i], inplace=True)

        
    df_input_ml = df_input[['gest-cs_gestant', 'gest-cs_raca',
                            'gest-cs_escol_n', 'gest-tpevidenci',
                            'gest-tpesquema', 'gest-tratparc',
                            'gest-tpesqpar', 'sinasc-sexo',
                            'sinasc-peso', 'sinasc-apgar1', 'sinasc-apgar5',
                            'sinasc-idademae', 'sinasc-qtdfilvivo',
                            'sinasc-qtdfilmort', 'sinasc-gestacao', 'sinasc-consultas',
                            'sinasc-gravidez', 'sinasc-parto', 'sinasc-endnasc',
                            'congenita_diagnosis',
                            'gest-sem_diag']]

for c in list(df_input_ml.columns):
    print("{}: {}".format(c, df_input_ml[c].isna().sum()))

def read_and_get_input(columns_name, bairros_que_ficam, categoricos, unidades_saude_que_ficam,
                       clean_ignored_value=False, clean_nan_value=False):

    def take_off_ignored_values(df_input_ml):

        # Mantém somente os registros com informações do trimestre, removido os ignorados e não aplicados
        df_input_ml = df_input_ml[df_input_ml['gest-cs_gestant'].isin([1, 2, 3])]

        # Mantém somente os registro que possuem dado de raça
        df_input_ml['gest-cs_raca'] = df_input_ml['gest-cs_raca'].apply(lambda x: float(str(x).replace("_", "9")))
        df_input_ml = df_input_ml[df_input_ml['gest-cs_raca'].isin(list(range(1,6)))]

        # Mantém somente os registro que possuem dado de escolaridade
        df_input_ml['gest-cs_escol_n'] = df_input_ml['gest-cs_escol_n'].apply(
                            lambda x: float(str(x).replace("_", "")))
        df_input_ml = df_input_ml[df_input_ml['gest-cs_escol_n'].isin(list(range(1,9)))]

        # Foi removido que tinha o código de informação ignorada
        df_input_ml = df_input_ml[df_input_ml['gest-tpevidenci'].isin(list(range(1,5)))]

        # Foi removido que tinha o código de informação ignorada e não realizado
        df_input_ml = df_input_ml[~df_input_ml['gest-tpesquema'].isin([9, 5])]

        # Foi removido que tinha o código de informação ignorada
        df_input_ml = df_input_ml[~df_input_ml['gest-tratparc'].isin([9])]

         # Foi removido que tinha o código de informação ignorada e não realizado
        df_input_ml = df_input_ml[~df_input_ml['gest-tpesqpar'].isin([9, 5])]

        # Foi retirado os de valores 1 que significa ignorado
        df_input_ml = df_input_ml[~df_input_ml['gest-tpesquema'].isin([1])]

        return df_input_ml

    def take_off_na_values(df_input_ml):

        # Foi removido todos os valores nulos
        df_input_ml = df_input_ml[~df_input_ml['sinasc-peso'].isna()]
        df_input_ml = df_input_ml[~df_input_ml['sinasc-apgar1'].isna()]
        df_input_ml = df_input_ml[~df_input_ml['sinasc-apgar5'].isna()]
        df_input_ml = df_input_ml[~df_input_ml['sinasc-qtdfilvivo'].isna()]
        df_input_ml = df_input_ml[~df_input_ml['sinasc-qtdfilmort'].isna()]
        df_input_ml = df_input_ml[~df_input_ml['sinasc-qtdfilmort'].isna()]
        df_input_ml = df_input_ml[~df_input_ml['sinasc-gestacao'].isna()]
        df_input_ml = df_input_ml[~df_input_ml['sinasc-consultas'].isna()]
        df_input_ml = df_input_ml[~df_input_ml['sinasc-gravidez'].isna()]
        df_input_ml = df_input_ml[~df_input_ml['sinasc-parto'].isna()]
        df_input_ml = df_input_ml[~df_input_ml['sinasc-idanomal'].isna()]
        df_input_ml = df_input_ml[~df_input_ml['sinasc-bainasc'].isna()]

        return df_input_ml

    df_input = pd.read_csv(base_path + '/data_sus/finais/sifilis_match_input_ml_original.csv')

    df_input_ml = df_input.drop(columns=['gest-ID_SINAN_SYPHILIS_GESTATIONAL', 'gest-ID_SINAN_SYPHILIS',
                                         'gest-syphilis_diagnosis', 'ID_SINASC', 'ID_SIM',
                                         'ID_SINAN_SYPHILIS_GESTATIONAL',
                                         'ID_SINAN_SYPHILIS_CONGENITAL', 'Unnamed: 0', 'gest-tpconfirma',
                                         'gest-pre_munire', 'gest-pre_ufrel', 'nasc_motherID', 'sinasc-ID_SINASC',
                                         'sinasc-codocupmae', 'gest-geo_sifil_name_neighborhood', 'gest_mother_ID',
                                         'nasc_singleton', 'gest_singleton'])

    # df_input_ml['gest-sem_diag'] = df_input_ml['gest-sem_diag'].apply(lambda x: int(str(x)[-2:]))

    if clean_ignored_value:
        df_input_ml = take_off_ignored_values(df_input_ml)

    if clean_nan_value:
        df_input_ml = take_off_na_values(df_input_ml)
    else:
        df_input_ml = df_input_ml.fillna(method='ffill')

    df_input_ml['gest-pre_unipre'] = df_input_ml['gest-pre_unipre'].apply(lambda x: str(int(x)))

    # Trocando nome das colunas
    df_input_ml.columns = columns_name
    columns_update = columns_name.copy()

    # Tratativa de variáveis categoricas de estabelecimentos
    if not unidades_saude_que_ficam == False:
        df_input_ml = pd.get_dummies(df_input_ml, columns=['cod_unidade_nasc'])
        columns_update.remove('cod_unidade_nasc')
        if not unidades_saude_que_ficam == []:
            df_input_ml = df_input_ml[columns_update + unidades_saude_que_ficam]
            columns_update.extend(unidades_saude_que_ficam)
        else:
            columns_update = list(df_input_ml.columns)
    else:
        df_input_ml.drop(columns=['cod_unidade_nasc'], inplace=True)

    # Tratativa de variáveis categoricas de bairros
    if not bairros_que_ficam == False:
        df_input_ml = pd.get_dummies(df_input_ml, columns=['bairro_nascimento'])
        if not bairros_que_ficam == []:
            columns_update.remove('bairro_nascimento')
            df_input_ml = df_input_ml[columns_update + bairros_que_ficam]
    else:
        df_input_ml.drop(columns=['bairro_nascimento'], inplace=True)

    # Dealing with categorical
    df_input_ml = pd.get_dummies(df_input_ml, columns=categoricos)

    return df_input_ml


    '''
    Base balanceada
    Sem variável de semana
    Não limpa : 13183 registros
    '''
    # ######################### randomForest ###########################
    # Acurácia: (0.64, 0.09)
    # Precisão: (0.65, 0.11)
    # Cobertura: (0.63, 0.19)
    # Métrica-F1: (0.63, 0.13)

    ######################### lightGBM ###########################
    # Acurácia: (0.62, 0.06)
    # Precisão: (0.61, 0.06)
    # Cobertura: (0.63, 0.16)
    # Métrica - F1: (0.61, 0.1)

    ######################### xgBoost ###########################
    # Acurácia: (0.59, 0.06)
    # Precisão: (0.58, 0.06)
    # Cobertura: (0.62, 0.13)
    # Métrica - F1: (0.6, 0.09)

    '''
    Base balanceada
    Com variável de semana
    Não limpa : 13183 registros
    '''
    ######################### randomForest ###########################
    # Acurácia: (0.71, 0.12)
    # Precisão: (0.76, 0.13)
    # Cobertura: (0.62, 0.28)
    # Métrica - F1: (0.65, 0.21)

    # ######################### lightGBM ###########################
    # Acurácia: (0.69, 0.1)
    # Precisão: (0.68, 0.08)
    # Cobertura: (0.67, 0.23)
    # Métrica-F1: (0.66, 0.15)

    ######################### xgBoost ###########################
    # Acurácia: (0.65, 0.09)
    # Precisão: (0.64, 0.08)
    # Cobertura: (0.66, 0.21)
    # Métrica - F1: (0.64, 0.14)

    '''
    Base balanceada
    Com variável de semana
    Total Limpa : 1881 registros
    '''
    ######################### randomForest ###########################
    # Acurácia: (0.63, 0.18)
    # Precisão: (0.68, 0.19)
    # Cobertura: (0.62, 0.13)
    # Métrica - F1: (0.64, 0.13)

    ######################### lightGBM ###########################
    # Acurácia: (0.57, 0.12)
    # Precisão: (0.58, 0.12)
    # Cobertura: (0.57, 0.14)
    # Métrica - F1: (0.57, 0.1)

    ######################### xgBoost ###########################
    # Acurácia: (0.61, 0.12)
    # Precisão: (0.62, 0.11)
    # Cobertura: (0.64, 0.1)
    # Métrica - F1: (0.62, 0.08)

    '''
    Base balanceada
    Sem variável de semana
    Total Limpa : 1881 registros
    '''
    ######################### randomForest ###########################
    # Acurácia: (0.6, 0.09)
    # Precisão: (0.6, 0.09)
    # Cobertura: (0.61, 0.14)
    # Métrica - F1: (0.6, 0.1)

    ######################### lightGBM ###########################
    # Acurácia: (0.54, 0.09)
    # Precisão: (0.55, 0.1)
    # Cobertura: (0.56, 0.11)
    # Métrica - F1: (0.55, 0.09)

    ######################### xgBoost ###########################
    # Acurácia: (0.58, 0.07)
    # Precisão: (0.59, 0.08)
    # Cobertura: (0.6, 0.08)
    # Métrica - F1: (0.59, 0.06)

    '''
    Base balanceada
    Com variável de semana
    Limpa somente valores na : 9203 registros
    '''
    ######################### randomForest ###########################
    # Acurácia: (0.6, 0.09)
    # Precisão: (0.6, 0.09)
    # Cobertura: (0.61, 0.14)
    # Métrica - F1: (0.6, 0.1)

    ######################### lightGBM ###########################
    # Acurácia: (0.54, 0.09)
    # Precisão: (0.55, 0.1)
    # Cobertura: (0.56, 0.11)
    # Métrica - F1: (0.55, 0.09)

    ######################### xgBoost ###########################
    # Acurácia: (0.58, 0.07)
    # Precisão: (0.59, 0.08)
    # Cobertura: (0.6, 0.08)
    # Métrica - F1: (0.59, 0.06)

    '''
    Base balanceada
    Com variável de semana
    Limpa somente valores ignored : 2527 registros
    '''
    ######################### randomForest ###########################
    # Acurácia: (0.64, 0.1)
    # Precisão: (0.64, 0.1)
    # Cobertura: (0.63, 0.17)
    # Métrica - F1: (0.63, 0.13)

    ######################### lightGBM ###########################
    # Acurácia: (0.62, 0.07)
    # Precisão: (0.61, 0.06)
    # Cobertura: (0.66, 0.13)
    # Métrica-F1: (0.63, 0.09)

    ######################### xgBoost ###########################
    # Acurácia: (0.62, 0.11)
    # Precisão: (0.61, 0.11)
    # Cobertura: (0.65, 0.14)
    # Métrica - F1: (0.63, 0.12)

    # df 0
    # 2016 1376
    # 2017 1187
    # 2015 1060
    # 2014 776
    # 2013 564
    # 2012 432
    # 2011 232
    # 2010 117
    #
    # Acurácia: (0.62, 0.02)
    # Precisão: (0.59, 0.01)
    # Cobertura: (0.78, 0.03)
    # Métrica - F1: (0.67, 0.02)

    # df 1
    # 2016 1688
    # 2017 1412
    # 2015 1387
    # 2014 1034
    # 2013 717
    # 2012 497
    # 2011 293
    # 2010 152
    #
    # Acurácia: (0.63, 0.01)
    # Precisão: (0.6, 0.01)
    # Cobertura: (0.78, 0.03)
    # Métrica - F1: (0.68, 0.01)