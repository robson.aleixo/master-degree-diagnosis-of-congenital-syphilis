#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 13:40:29 2020

@author: robson
"""
import pandas as pd
import numpy as np
import os
import re

from machine_learning.mlMethods import *
from machine_learning.mlSupportMethods import *

pd.options.mode.chained_assignment = None

'''
'sinasc-gestacao' essa variável mostra que tem casos de gestação inicial, verificar como essa base considera diversas
consultas da mesma pessoa.

'gest-cs_gestant' - Indica em qual trimestre está a gestação
'gest-cs_raca' - 1: branca, 2: preta, 3: amarela, 4: parda e 5: indígena
'gest-cs_escol_n' - nível de escolaridade da gestante 
'gest-tpevidenci' - Nível de evidência da Sifilis
'gest-tpesquema' - Esquema de tratamento de penicilina 
'gest-tratparc' - Booleana que indica se o parceiro foi tratado ou não 
'gest-tpesqpar' - Esquema de tratamento de penicilina do parceiro
'sinasc-sexo' - sexo da criança
'sinasc-peso' - peso da criança 
'sinasc-apgar1' - teste de vitalidade da criança no primeiro minuto 
'sinasc-apgar5' - teste de vitalidade da criança no quinto minuto
'sinasc-idademae' - idade da mãe 
'sinasc-qtdfilvivo' - quantidade de filhos vivos
'sinasc-qtdfilmort'- quantidade de filhos mortos 
'sinasc-gestacao' - semana de gestação 1: Menos de 22 semanas, 2: 22 a 27 semanas, 
                    3: 28 a 31 semanas, 4: 32 a 36 semanas, 5: 37 a 41 semanas, 
                    6: 42 semanas e mais.
'sinasc-consultas' - quantidade de consultas pré-natal
'sinasc-gravidez' - número de bebes na gestação
'sinasc-parto' - tipo de parto, se vaginal ou cesáreo 
'sinasc-endnasc' - bairro de nascimento
'congenita_diagnosis' - diagnóstico de sifilis
'gest-sem_diag' - semana do ano em que ocorreu o diagnóstico de sifilis 

'''

os.chdir('/home/robson/Documents/Mestrado/Pesquisa/mestrado_diagnostico_doenca')

base_path = os.getcwd()

categoricos = ['periodo_gestacao', 'etnia_mae', 'nivel_escolaridade', 'cod_unidade_nasc',
               'nivel_sifilis_mae', 'teste_treponemico_mae', 'esquema_tratamento_mae',
               'parceiro_tratado', 'esquema_tratamento_parce', 'sexo_crianca', 'teste_apgar1', 'teste_apgar5',
               'tipo_parto', 'bairro_nascimento']

bairros_que_ficam = ['bairro_nascimento_CAMPO GRANDE',
     'bairro_nascimento_CENTRO', 'bairro_nascimento_ACARI', 'bairro_nascimento_REALENGO',
     'bairro_nascimento_SAO CRISTOVAO', 'bairro_nascimento_BANGU', 'bairro_nascimento_LINS DE VASCONCELOS',
     'bairro_nascimento_BARRA DA TIJUCA', 'bairro_nascimento_SANTA CRUZ', 'bairro_nascimento_MAL HERMES']

columns_name = ['idade_mae', 'periodo_gestacao', 'etnia_mae', 'nivel_escolaridade', 'cod_unidade_nasc',
     'nivel_sifilis_mae', 'teste_treponemico_mae', 'esquema_tratamento_mae', 'parceiro_tratado',
     'esquema_tratamento_parce', 'semana_ano_diagnostico', 'sexo_crianca', 'peso_crianca', 'teste_apgar1',
     'teste_apgar5', 'qtdfilho_vivo', 'qtdfilho_mort', 'qtd_semana_gestacao', 'numero_consultas',
     'tipo_gravidez', 'tipo_parto', 'bairro_nascimento', 'diagnostico_congenita']

unidades_saude_que_ficam = ['cod_unidade_nasc_4046307', 'cod_unidade_nasc_2269988', 'cod_unidade_nasc_2269627',
                            'cod_unidade_nasc_2270439', 'cod_unidade_nasc_2269945', 'cod_unidade_nasc_2269902',
                            'cod_unidade_nasc_2273373', 'cod_unidade_nasc_2296543', 'cod_unidade_nasc_2270250',
                            'cod_unidade_nasc_6927254', 'cod_unidade_nasc_5717256', 'cod_unidade_nasc_2269783',
                            'cod_unidade_nasc_2273640', 'cod_unidade_nasc_2270579', 'cod_unidade_nasc_2296535']


def read_and_balance():

    def balance_the_base(df_input_ml):

        # vai até 17791, separar em 5 grupos de aproximadamente 3558
        df_input_ml_no = df_input_ml[df_input_ml['diagnostico_congenita'] == 0]
        df_input_ml_yes = df_input_ml[df_input_ml['diagnostico_congenita'] == 1]
        df_input_ml_no['id'] = list(range(0, df_input_ml_no.shape[0]))
        bucket = df_input_ml['diagnostico_congenita'].value_counts()[1]

        number_divided = int(df_input_ml['diagnostico_congenita'].value_counts()[0]/ \
                             df_input_ml['diagnostico_congenita'].value_counts()[1])

        bases_dict = dict()
        for i in list(range(0, number_divided+1)):
            if i == number_divided:
                bases_dict[str(i)] = df_input_ml_no.copy()
                bases_dict[str(i)] = bases_dict[str(i)].append(df_input_ml_yes)
            else:
                bases_dict[str(i)] = df_input_ml_no.sample(n=bucket, replace=False)
                bases_dict[str(i)] = bases_dict[str(i)].append(df_input_ml_yes)
                df_input_ml_no = df_input_ml_no[-df_input_ml_no['id'].isin(list(bases_dict[str(i)]['id']))]

        for df in list(bases_dict.keys()):
            bases_dict[df].drop(columns=['id'], inplace=True)

        return bases_dict

    df_input_ml = read_and_get_input(columns_name, categoricos)

    dfs_balanced = balance_the_base(df_input_ml)

    return dfs_balanced


def take_train_and_test(dfs_balanced):

    df_input_ml = dfs_balanced['0'].copy()
    df_input_ml.drop(columns=['ano'], inplace=True)

    df_input_ml_X = df_input_ml.drop(columns=['diagnostico_congenita'])
    df_input_ml_y = df_input_ml['diagnostico_congenita']

    X_train, X_test, y_train, y_test = train_test_split(df_input_ml_X,
                                                        df_input_ml_y,
                                                        test_size=0.2,
                                                        random_state=1)

    return X_train, y_train


def looking_for_best_features(m):

    dfs_balanced = read_and_balance()

    X_train, y_train = take_train_and_test(dfs_balanced)

    columns_result = ['Modelo', 'AUC', 'Quantidade de Variaveis']

    df_results = pd.DataFrame(columns=columns_result)

    auc, classifier = globals()["run_" + m](X_train, y_train)

    df_features = apply_shap_values(X_train, classifier, m)

    if m not in ['naive_bayes', 'knn']:
        df_features = apply_shap_values(X_train, classifier, m)

    X_train_copy = X_train.copy()

    i = 1

    while i <= 50:

        if m not in ['naive_bayes', 'knn']:
            X_train = X_train_copy.loc[:, list(df_features['feature'])[:i*10]]
        else:
            X_train = apply_kBest(X_train_copy, y_train, i*10)

        auc, classifier = globals()["run_" + m](X_train, y_train)

        aux = pd.DataFrame([[m, auc, X_train.shape[1]]], columns=columns_result)

        df_results = df_results.append(aux)

        i += 1

    return df_results


def basic_process():

    dfs_balanced = read_and_balance()

    models = ['logistic_regression', 'knn', 'svm_linear',
              'svm_rbf', 'naive_bayes', 'random_classifier',
              'extratree_classifier', 'lightGBM_classifier']

    df_result = pd.DataFrame(columns=['AUC', 'Precision', 'Recall', 'F1-Score', 'Model'])

    for m in models:

        try:
            for i in list(dfs_balanced.keys()):

                df_input_ml = dfs_balanced[i].copy()

                df_input_ml.drop(columns=['ano'], inplace=True)

                df_input_ml_X = df_input_ml.drop(columns=['diagnostico_congenita'])
                df_input_ml_y = df_input_ml['diagnostico_congenita']

                X_train, X_test, y_train, y_test = train_test_split(df_input_ml_X,
                                                                    df_input_ml_y,
                                                                    test_size = 0.2,
                                                                    random_state = 1)

                auc, classifier = globals()["run_{}".format(m)](X_train, y_train, grid_search=False)

                y_pred = classifier.predict(X_test)
                df = show_results_return_df(y_test, y_pred)
                df['Model'] = [m] * df.shape[0]
                df_result = df_result.append(df)
        except Exception as ex:
            continue

        df_result.to_csv(base_path + \
            '/data_sus/finais/Resultados_Desempenho_Geral/resultado_geral_{}.csv'.format(m))




def shap_best_features(m):

    # models = ['logistic_regression', 'knn', 'svm', 'kernel_svm', 'naive_bayes',
    #           'random_classifier', 'extratree_classifier', 'lightGBM_classifier',
    #           'xgboost_classifier']
    # m = 'logistic_regression'
    # m = 'knn'
    # m = 'svm'
    # m = 'kernel_svm'
    # m = 'naive_bayes'
    # m = 'random_classifier'
    # m = 'extratree_classifier'
    # m = 'lightGBM_classifier'
    # m = 'xgboost_classifier'

    dfs_balanced = read_and_balance()

    df_input_ml = dfs_balanced['0'].copy()

    df_input_ml.drop(columns=['ano'], inplace=True)

    df_input_ml_X = df_input_ml.drop(columns=['diagnostico_congenita'])
    df_input_ml_y = df_input_ml['diagnostico_congenita']

    X_train, X_test, y_train, y_test = train_test_split(df_input_ml_X,
                                                        df_input_ml_y,
                                                        test_size=0.2,
                                                        random_state=1)

    auc, classifier = globals()["run_" + m](X_train, y_train)
    df = apply_shap_values(X_train, classifier, m)

    return df


def execute_grid_after_feature_selection(m):

    def take_num_features(df):

        num = 0
        auc = 0
        for i in list(range(df.shape[0])):
            if df.iloc[i, list(df.columns).index('ACU')] > auc:
                auc = df.iloc[i, list(df.columns).index('ACU')]
                num = df.iloc[i, list(df.columns).index('Quantidade de Variaveis')]

        return num

    # Lê base de dados e seleciona a base de treino
    dfs_balanced = read_and_balance()

    X_train, y_train = take_train_and_test(dfs_balanced)

    # Executa o modelo com os melhores parâmetros
    auc, classifier = globals()["run_" + m](X_train, y_train)

    # Calcula quais são as melhores features e lê o df com os resultados por seleção
    # df_features = apply_shap_values(X_train, classifier, m)
    #
    # df_features.to_csv(base_path + \
    #                  "/data_sus/finais/analise_variaveis/feature_importance_{}.csv".format(m))

    df_features = pd.read_csv(base_path + \
                       "/data_sus/finais/analise_variaveis/feature_importance_{}.csv".format(m))

    df_asses_features = pd.read_csv(base_path + \
                     "/data_sus/finais/analise_variaveis/auc_qtd_features/auc_qtd_features_{}.csv".format(m))

    # Pega a quantidade ideal de features baseado no df com as avaliações
    num = take_num_features(df_asses_features)

    # Separa
    X_train = X_train[list(df_features.iloc[:num, list(df_features.columns).index('feature')])]
    auc, classifier = globals()["run_" + m](X_train, y_train, grid_search=True)


if __name__ == '__main__':

    m = 'logistic_regression'
    m = 'knn'
    m = 'svm_linear'
    m = 'svm_rbf'
    m = 'naive_bayes'
    m = 'random_classifier'
    m = 'extratree_classifier'
    m = 'lightGBM_classifier'
    m = 'xgboost_classifier'

    shap_best_features(m)

    df_results = looking_for_best_features(m)

    df_results.to_csv(base_path + "/data_sus/finais/auc_qtd_features_{}.csv".format(m))
