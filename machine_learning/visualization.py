from matplotlib import pyplot as plt
import seaborn as sns
import pandas as pd
from matplotlib.colors import ListedColormap
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.tree import export_graphviz
from IPython.display import Image
from subprocess import call
import os

os.chdir('/home/robson/Documents/Mestrado/Pesquisa/mestrado_diagnostico_doenca')

base_path = os.getcwd()


def boxplot_peso_crianca(df_input_ml):

    for c in list(df_input_ml.columns):
        if c not in ['diagnostico_congenita', 'bairro_nascimento', 'peso_crianca', 'idade_mae']:
            aux = df_input_ml[~df_input_ml[c].isna()]
            if c not in ['qtdfilho_vivo', 'qtdfilho_mort']:
                aux = aux[~aux[c].isin([9, 10, "I"])]
            sns.boxplot(x=c, y='peso_crianca',
                        hue='diagnostico_congenita',
                        data=aux)
            plt.show()


def boxplot_idade_mae(df_input_ml):
    for c in list(df_input_ml.columns):
        if c not in ['diagnostico_congenita', 'bairro_nascimento', 'peso_crianca', 'idade_mae']:
            aux = df_input_ml[~df_input_ml[c].isna()]
            if c not in ['qtdfilho_vivo', 'qtdfilho_mort']:
                aux = aux[~aux[c].isin([9, 10, "I"])]
            sns.boxplot(x=c, y='idade_mae',
                        hue='diagnostico_congenita',
                        data=aux)
            plt.show()


def relplot_etnia(df_input_ml):
    for c in list(df_input_ml.columns):
        if c not in ['diagnostico_congenita', 'bairro_nascimento', 'peso_crianca', 'idade_mae']:
            aux = df_input_ml[~df_input_ml[c].isna()]
            if c not in ['qtdfilho_vivo', 'qtdfilho_mort']:
                aux = aux[~aux[c].isin([9, 10, "I"])]
            sns.relplot(x='idade_mae', y='peso_crianca',
                        col=c, hue='diagnostico_congenita', alpha=0.5,
                        data=df_input_ml)
            plt.show()

            jh = df_input_ml[df_input_ml['congenita_diagnosis'] == 0]
            sns.jointplot(x='gest-nu_idade_n', y='sinasc-peso',
                          data=jh)
            plt.show()


def violin_etnia(df_input_ml):
    for c in list(df_input_ml.columns):
        if c not in ['diagnostico_congenita', 'bairro_nascimento', 'peso_crianca', 'idade_mae']:
            aux = df_input_ml[~df_input_ml[c].isna()]
            if c not in ['qtdfilho_vivo', 'qtdfilho_mort']:
                aux = aux[~aux[c].isin([9, 10, "I"])]
            sns.violinplot(x=c, y='peso_crianca',
                        hue='diagnostico_congenita',
                        data=df_input_ml)
            plt.show()


def pairgrid(df_input_ml):
    df = df_input_ml.drop(columns=['bairro_nascimento'])

    g = sns.PairGrid(df)
    g.map(sns.scatterplot)
    plt.show()


def analise_bairro(df_input_ml):

    df = pd.pivot_table(index=['bairro_nascimento'],
                        values=['diagnostico_congenita'],
                        aggfunc="sum",
                        data=df_input_ml)
    df = df.reset_index()
    df.columns = ['Bairro', 'Número de Casos de Sífilis Congênita']

    df_aux = df_input_ml['bairro_nascimento'].value_counts()
    df_aux = df_aux.reset_index()
    df_aux.columns = ['Bairro', 'Total de Registros']

    df = pd.merge(df, df_aux, on="Bairro")
    df["porcent"] = df[['Número de Casos de Sífilis Congênita',
                        'Total de Registros']].apply(lambda x: round(x[0]/x[1], 2),
                                                     axis=1)

    df.sort_values(by=['Número de Casos de Sífilis Congênita'], ascending=False,
                   inplace=True)
    df = df.iloc[:10,]

    fig = plt.figure() # Create matplotlib figure

    ax = fig.add_subplot(111) # Create matplotlib axes
    ax2 = ax.twinx() # Create another axes that shares the same x-axis as ax.

    fig, ax = plt.subplots(figsize=(20, 10))
    ax2 = ax.twinx()

    width = 0.4

    df['Número de Casos de Sífilis Congênita'].plot(kind='bar', color='red',
                                                    ax=ax, width=width, position=1)
    df["porcent"].plot(kind='bar', color='blue', ax=ax2, width=width, position=0)

    # ax.set_xlabel(df["Bairro"])
    ax.set_ylabel('Número de Casos de Sífilis Congênita')
    ax2.set_ylabel("Porcentagem de casos positivos")
    ax2.set_ylim((0,0.5))

    plt.show()


def histograma_idade_mae(df_input_ml):

    df = df_input_ml[['idade_mae', 'diagnostico_congenita']]
    df.columns = ['Idade da Mãe', 'Diagnostico de Sífilis Congênita']

    sns.displot(x='Idade da Mãe', hue='Diagnostico de Sífilis Congênita',
                kind='kde', fill=True,
                data=df)

    # sns.displot(x='Idade da Mãe', col='Diagnostico de Sífilis Congênita',
    #             shrink=1.2, bins=40,
    #               data=df)

    plt.show()


def histograma_peso_crianca(df_input_ml):

    df = df_input_ml[['peso_crianca', 'diagnostico_congenita']]
    df.columns = ['Peso da Criança (gramas)', 'Diagnostico de Sífilis Congênita']

    # sns.displot(x='Peso da Criança (gramas)',
    #             col='Diagnostico de Sífilis Congênita', shrink=1.2, bins=40,
    #             data=df)

    sns.displot(x='Peso da Criança (gramas)', hue='Diagnostico de Sífilis Congênita',
                kind='kde', fill=True,
                data=df)
    plt.show()


def bairro_visualization(df_input_ml):

    df = pd.pivot_table(index=['semana_ano_diagnostico'],
                        values=['diagnostico_congenita'],
                        aggfunc="sum",
                        data=df_input_ml)
    df = df.reset_index()
    df.columns = ['semana_ano_diagnostico', 'Número de Casos de Sífilis Congênita']

    df_aux = df_input_ml['semana_ano_diagnostico'].value_counts()
    df_aux = df_aux.reset_index()
    df_aux.columns = ['semana_ano_diagnostico', 'Total de Registros']

    df = pd.merge(df, df_aux, on='semana_ano_diagnostico')
    df["porcent"] = df[['Número de Casos de Sífilis Congênita',
                        'Total de Registros']].apply(lambda x: round(x[0] / x[1], 2),
                                                     axis=1)

    df.sort_values(by=['semana_ano_diagnostico'], ascending=True,
                   inplace=True)

    df_print = df.copy()

    fig = plt.figure() # Create matplotlib figure

    ax = fig.add_subplot(111) # Create matplotlib axes
    ax2 = ax.twinx() # Create another axes that shares the same x-axis as ax.

    fig, ax = plt.subplots(figsize=(20, 10))
    ax2 = ax.twinx()

    width = 0.4

    df_print['Número de Casos de Sífilis Congênita'].plot(kind='bar', color='red',
                                                    ax=ax, width=width, position=1)
    df_print["porcent"].plot(kind='bar', color='blue', ax=ax2, width=width, position=0)

    # ax.set_xlabel(df["Bairro"])
    ax.set_ylabel('Número de Casos de Sífilis Congênita')
    ax2.set_ylabel("Porcentagem de casos positivos")
    ax2.set_ylim((0,0.8))

    plt.show()


def uni_saude_visualization(df_input_ml):

    df = pd.pivot_table(index=['cod_unidade_nasc'],
                        values=['diagnostico_congenita'],
                        aggfunc="sum",
                        data=df_input_ml)
    df = df.reset_index()
    df.columns = ['unidade_saude', 'Número de Casos de Sífilis Congênita']

    df_aux = df_input_ml['cod_unidade_nasc'].value_counts()
    df_aux = df_aux.reset_index()
    df_aux.columns = ['unidade_saude', 'Total de Registros']

    df = pd.merge(df, df_aux, on='unidade_saude')
    df["porcent"] = df[['Número de Casos de Sífilis Congênita',
                        'Total de Registros']].apply(lambda x: round(x[0] / x[1], 2),
                                                     axis=1)

    df.sort_values(by=['Número de Casos de Sífilis Congênita'], ascending=False,
                   inplace=True)

    df_print = df.iloc[:50,:]

    fig = plt.figure() # Create matplotlib figure

    ax = fig.add_subplot(111) # Create matplotlib axes
    ax2 = ax.twinx() # Create another axes that shares the same x-axis as ax.

    fig, ax = plt.subplots(figsize=(20, 10))
    ax2 = ax.twinx()

    width = 0.4

    df_print['Número de Casos de Sífilis Congênita'].plot(kind='bar', color='red',
                                                    ax=ax, width=width, position=1)
    df_print["porcent"].plot(kind='bar', color='blue', ax=ax2, width=width, position=0)

    # ax.set_xlabel(df_print['unidade_saude'])
    ax.set_xticks(list(df_print['unidade_saude']))
    ax2.set_ylabel("Porcentagem de casos positivos")
    ax2.set_ylim((0,1))

    plt.show()


def output_classification(X_test, y_test):

    X_set, y_set = X_test, y_test
    X1, X2 = np.meshgrid(np.arange(start=X_set[:, list(X_set.columns).index()].min() - 1,
                                   stop=X_set[:, list(X_set.columns).index()].max() + 1, step=0.01),
                         np.arange(start=X_set[:, list(X_set.columns).index()].min() - 1,
                                   stop=X_set[:, list(X_set.columns).index()].max() + 1, step=0.01))
    plt.contourf(X1, X2, classifier.predict(np.array([X1.ravel(), X2.ravel()]).T).reshape(X1.shape),
                 alpha=0.75, cmap=ListedColormap(('red', 'green')))
    plt.xlim(X1.min(), X1.max())
    plt.ylim(X2.min(), X2.max())
    for i, j in enumerate(np.unique(y_set)):
        plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, 1],
                    c=ListedColormap(('red', 'green'))(i), label=j)
    plt.title('Randon Forest Classification')
    plt.xlabel('Age')
    plt.ylabel('Estimated Salary')
    plt.legend()
    plt.show()


def plot_roc_curve(false_positive_rate, true_positive_rate, roc_auc):

    plt.figure()
    lw = 2
    plt.plot(false_positive_rate, true_positive_rate, color='darkorange',
             lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()


def plot_tree(classifier, df_input_ml_X):

    estimator = classifier.estimators_[5]

    # Export as dot file
    export_graphviz(estimator, out_file='tree.dot',
                    feature_names=list(df_input_ml_X.columns),
                    class_names=['0', '1'],
                    rounded=True, proportion=False,
                    precision=2, filled=True)

    call(['dot', '-Tpng', 'tree.dot',
          '-o', 'tree.png', '-Gdpi=600'])

    # Display in jupyter notebook
    Image(filename='tree.png')


def plot_auc_selection_features():

    # models = ['logistic_regression', 'knn', 'svm_linear',
    #           'svm_rbf', 'naive_bayes', 'random_classifier',
    #           'extratree_classifier', 'lightGBM_classifier',
    #           'xgboost_classifier']

    models = ['logistic_regression', 'knn', 'naive_bayes', 'random_classifier',
              'extratree_classifier', 'lightGBM_classifier',
              'xgboost_classifier']

    df_consolidade = pd.DataFrame(columns=['Modelo', 'ACU', 'Quantidade de Variaveis'])

    for m in models:
        df = pd.read_csv(base_path + "/data_sus/finais/analise_variaveis/auc_qtd_features_{}.csv".format(m))
        df = df.iloc[:10, :]
        df.drop(columns=['Unnamed: 0'], inplace=True)
        df_consolidade = df_consolidade.append(df)

    df_consolidade.columns = ['Modelo', 'AUC', 'Quantidade de Variaveis']

    sns.set_theme(style="whitegrid")

    for m in list(df_consolidade['Modelo'].unique()):

        df = df_consolidade[df_consolidade['Modelo'] == m]
        f, ax = plt.subplots(figsize=(8, 6))
        sns.set_context(font_scale=0.9)
        sns.lineplot(x='Quantidade de Variaveis', y='AUC',
                    data=df, linewidth=5.0)
        plt.title(m)
        ax.set_ylim((0.55, 0.7))
        plt.show()


def plot_feature_importance(df):

    # models = ['logistic_regression', 'knn', 'svm_linear',
    #           'svm_rbf', 'naive_bayes', 'random_classifier',
    #           'extratree_classifier', 'lightGBM_classifier',
    #           'xgboost_classifier']

    models = ['random_classifier',
              'extratree_classifier',
              'xgboost_classifier']

    for m in models:
        df = pd.read_csv(base_path + "/data_sus/finais/analise_variaveis/feature_importance_{}.csv".format(m))
        df = df.iloc[:10, :]
        df.drop(columns=['Unnamed: 0'], inplace=True)

        sns.set_theme(style="white")
        f, ax = plt.subplots(figsize=(8, 6))
        sns.set_context(font_scale=0.9)
        sns.barplot(x='importance', y='feature',
                    data=df)
        plt.title(m)
        plt.show()