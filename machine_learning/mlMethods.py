from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import BernoulliNB
from sklearn.ensemble import ExtraTreesClassifier
from xgboost import XGBClassifier
from lightgbm import LGBMClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

from machine_learning.mlSupportMethods import *


def run_logistic_regression(X_train, y_train, grid_search=False):

    if grid_search:
        parameters = {'fit_intercept': [True, False],
                  'class_weight': [{0: 1, 1: 1}, {0: 1, 1: 1.3}],
                  'solver': ['newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga'],
                  'warm_start': [True, False]}

        classifier = LogisticRegression(random_state=0)

        parameters = apply_grid_search(X_train, y_train, classifier, parameters)
    else:
        # best_precision: 0.66
        parameters = {'class_weight': {0: 1, 1: 1.3}, 'fit_intercept': True,
                          'solver': 'liblinear', 'warm_start': True}

    classifier = LogisticRegression(fit_intercept=parameters['fit_intercept'],
                                    class_weight=parameters['class_weight'],
                                    solver=parameters['solver'],
                                    warm_start=parameters['warm_start'],
                                    random_state=0)

    classifier.fit(X_train, y_train)

    auc = apply_cross_validate(X_train, y_train, classifier, 'logistic_regression')

    return auc, classifier


def run_knn(X_train, y_train, grid_search=False):

    if grid_search:
        parameters = {'n_neighbors': [5, 10, 15],
                      'weights': ['uniform', 'distance'],
                      'algorithm': ['ball_tree', 'kd_tree', 'brute'],
                      'p': [1, 2]}

        classifier = KNeighborsClassifier()

        parameters = apply_grid_search(X_train, y_train, classifier, parameters)
    else:
        # best_precision: 0.58
        parameters = {'algorithm': 'kd_tree', 'n_neighbors': 5, 'p': 1, 'weights': 'distance'}

    # Fitting classifier to the Training set
    classifier = KNeighborsClassifier(n_neighbors=parameters['n_neighbors'],
                                      weights=parameters['weights'],
                                      algorithm=parameters['algorithm'],
                                      p=parameters['p'])

    classifier.fit(X_train, y_train)
    auc = apply_cross_validate(X_train, y_train, classifier, 'knn')

    return auc, classifier


def run_svm_linear(X_train, y_train, grid_search=False):

    if grid_search:
        parameters = {'kernel': ['linear'],
                      'C': [0.01, 1, 100],
                      'class_weight': [{0: 1, 1: 1}, {0: 1, 1: 1.3}]}

        classifier = SVC(kernel='linear', random_state=0)

        parameters = apply_grid_search(X_train, y_train, classifier, parameters)
    else:
        # best_precision: 0.65
        parameters = {'C': 1, 'class_weight': {0: 1, 1: 1}, 'kernel': 'linear'}

    classifier = SVC(kernel='linear',
                     C=parameters['C'],
                     class_weight=parameters['class_weight'],
                     probability=True,
                     random_state=0)

    classifier.fit(X_train, y_train)
    auc = apply_cross_validate(X_train, y_train, classifier, 'svn_linear')

    return auc, classifier


def run_svm_rbf(X_train, y_train, grid_search=False):

    if grid_search:
        parameters = {'kernel': ['rbf'],
                      'C': [0.01, 1, 100],
                      'gamma': [0.1, 1, 100],
                      'verbose': [True, False],
                      'class_weight': [{0: 1, 1: 1}, {0: 1, 1: 1.3}]}

        classifier = SVC(kernel='rbf', random_state=0)

        parameters = apply_grid_search(X_train, y_train, classifier, parameters)
    else:
        # best_precision: 0.58
        parameters = {'C': 100, 'class_weight': {0: 1, 1: 1.3}, 'gamma': 0.1,
                          'kernel': 'rbf', 'verbose': True}

    classifier = SVC(kernel='rbf',
                     C=parameters['C'],
                     gamma=parameters['gamma'],
                     class_weight=parameters['class_weight'],
                     verbose=parameters['verbose'],
                     probability=True,
                     random_state=0)

    classifier.fit(X_train, y_train)
    auc = apply_cross_validate(X_train, y_train, classifier, 'rbf')

    return auc, classifier


def run_naive_bayes(X_train, y_train, grid_search=False):

    if grid_search:
        parameters = {'alpha': [0, 1, 2, 5, 10],
                      'fit_prior': [True, False]}

        classifier = BernoulliNB()

        parameters = apply_grid_search(X_train, y_train, classifier, parameters)
    else:
        # best_precision: 0.64
        parameters = {'alpha': 2, 'binarize': 0, 'fit_prior': True}

    classifier = BernoulliNB(alpha=parameters['alpha'],
                             binarize=parameters['binarize'],
                             fit_prior=parameters['fit_prior'])

    classifier.fit(X_train, y_train)
    auc = apply_cross_validate(X_train, y_train, classifier, 'naive_bayes')

    return auc, classifier


def run_random_classifier(X_train, y_train, grid_search=False):

    if grid_search:

        parameters = [{'max_leaf_nodes': [10, 100, 500, 1000],
                       'criterion': ['entropy', 'gini'],
                       'n_estimators': [50, 100, 200],
                       'min_samples_split': [50, 100, 200],
                       'class_weight': [{0: 1, 1: 1}, {0: 1, 1: 1.3}],
                       'warm_start': [True, False]
                       }]

        classifier = RandomForestClassifier(random_state=0)
        #
        parameters = apply_grid_search(X_train, y_train, classifier, parameters)

    else:
        # best_precision: 0.68
        parameters = {'class_weight': {0: 1, 1: 1.3}, 'criterion': 'gini', 'max_leaf_nodes': 500,
                          'min_samples_split': 50, 'n_estimators': 200, 'warm_start': True}

    classifier = RandomForestClassifier(criterion=parameters['criterion'],
                                        n_estimators=parameters['n_estimators'],
                                        max_leaf_nodes=parameters['max_leaf_nodes'],
                                        min_samples_split=parameters['min_samples_split'],
                                        bootstrap=True,
                                        class_weight={0: 1, 1: 1.3},
                                        random_state=0)

    auc = apply_cross_validate(X_train, y_train, classifier, 'randomForest')

    classifier.fit(X_train, y_train)

    return auc, classifier


def run_extratree_classifier(X_train, y_train, grid_search=False):

    if grid_search:
        parameters = [{'max_leaf_nodes': [10, 100, 500, 1000],
                       'criterion': ['entropy', 'gini'],
                       'n_estimators': [50, 100, 200],
                       'min_samples_split': [50, 100, 200],
                       'class_weight': [{0: 1, 1: 1}, {0: 1, 1: 1.3}],
                       'warm_start': [True, False]
                       }]

        classifier = ExtraTreesClassifier(random_state=0)

        parameters = apply_grid_search(X_train, y_train, classifier, parameters)
    else:
        # best_precision: 0.66
        parameters = {'class_weight': {0: 1, 1: 1.3}, 'criterion': 'entropy', 'max_leaf_nodes': 100,
                          'min_samples_split': 50, 'n_estimators': 200, 'warm_start': True}

    classifier = ExtraTreesClassifier(criterion=parameters['criterion'],
                                        n_estimators=parameters['n_estimators'],
                                        max_leaf_nodes=parameters['max_leaf_nodes'],
                                        min_samples_split=parameters['min_samples_split'],
                                        bootstrap=True,
                                        class_weight={0: 1, 1: 1.3},
                                        random_state=0)
    classifier.fit(X_train, y_train)
    f1 = apply_cross_validate(X_train, y_train, classifier, 'extraTree')

    return f1, classifier


def run_lightGBM_classifier(X_train, y_train, grid_search=False):

    if grid_search:
        parameters = {
            'n_estimators': [100, 200, 500],
            'boosting': ['gbdt', 'rf', 'dart'],
            'bagging_freq': [1, 5, 10],
            'bagging_fraction':[0.5, 0.7, 0.9],
            'feature_fraction': [0.5, 0.7, 1]
        }
        classifier = LGBMClassifier(random_state=0)

        parameters = apply_grid_search(X_train, y_train, classifier, parameters)
    else:
        # best_precision: 0.68
        parameters = {'bagging_fraction': 0.9, 'bagging_freq': 1, 'boosting': 'dart',
                          'feature_fraction': 0.7, 'n_estimators': 500}

    classifier = LGBMClassifier(n_estimators=parameters['n_estimators'],
                                boosting=parameters['boosting'],
                                bagging_freq=parameters['bagging_freq'],
                                feature_fraction=parameters['feature_fraction'],
                                random_state=0)

    auc = apply_cross_validate(X_train, y_train, classifier, 'lightGBM')

    classifier.fit(X_train, y_train)

    return auc, classifier


def run_xgboost_classifier(X_train, y_train, grid_search=False):

    if grid_search:
        parameters = {
            'min_child_weight': [1, 5, 10],
            'gamma': [1, 2, 5],
            'subsample': [0.6, 0.7, 0.9],
            'colsample_bytree': [0.6, 0.7, 0.9],
            'n_estimators': [100, 300, 500],
            'tree_method': ['hist', 'auto', 'exact', 'approx'],
        }

        classifier = XGBClassifier(random_state=0)

        parameters = apply_grid_search(X_train, y_train, classifier, parameters)
    else:
        # best_precision: 0.68
        parameters = {'colsample_bytree': 0.6, 'gamma': 5, 'min_child_weight': 5,
        'n_estimators': 300, 'subsample': 0.9, 'tree_method': 'auto'}

    classifier = XGBClassifier(min_child_weight=parameters['min_child_weight'],
                               gamma=parameters['gamma'],
                               subsample=parameters['subsample'],
                               colsample_bytree=parameters['colsample_bytree'],
                               n_estimators=parameters['n_estimators'],
                               tree_method=parameters['tree_method'],
                               random_state=0)

    auc = apply_cross_validate(X_train, y_train, classifier, 'xgBoost')

    classifier.fit(X_train, y_train)

    return auc, classifier
