import shap


def apply_shap_values(classifier, df_input_ml_X):

    explainer = shap.TreeExplainer(classifier)
    shap_values = explainer.shap_values(df_input_ml_X)

    shap.summary_plot(shap_values, df_input_ml_X)