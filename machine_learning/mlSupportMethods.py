import pandas as pd
import os
import numpy as np
import re
from machine_learning.visualization import plot_roc_curve
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import cross_val_score, cross_validate
from sklearn.metrics import roc_curve, auc
import shap
from sklearn.feature_selection import SelectKBest, f_classif


os.chdir('/home/robson/Documents/Mestrado/Pesquisa/mestrado_diagnostico_doenca')

base_path = os.getcwd()


def read_and_get_input(columns_name, categoricos):

    def extra_cleaning(df_input_ml):

        df_input_ml['gest-sem_diag'] = df_input_ml['gest-sem_diag'].apply(lambda x: int(str(x)[-2:]))

        df_input_ml = df_input_ml[
            df_input_ml['gest-nu_idade_n'].isin(list(range(4010, 4100)))]

        df_input_ml['gest-nu_idade_n'] = df_input_ml['gest-nu_idade_n'].apply(lambda x: int(x) - 4000)

        df_input_ml['gest-cs_raca'] = df_input_ml['gest-cs_raca'].apply(lambda x: float(str(x).replace("_", "9")))

        df_input_ml['gest-cs_escol_n'] = df_input_ml['gest-cs_escol_n'].apply(
            lambda x: float(str(x).replace("_", "")))

        return df_input_ml

    df_input = pd.read_csv(base_path + '/data_sus/finais/sifilis_match_input_ml_original.csv')

    df_input_ml = df_input[['gest-nu_idade_n', 'gest-cs_gestant', 'gest-cs_raca',
                            'gest-cs_escol_n', 'gest-pre_unipre', 'gest-tpevidenci',
                            'gest-tpteste1', 'gest-tpesquema', 'gest-tratparc',
                            'gest-tpesqpar', 'gest-sem_diag', 'sinasc-sexo',
                            'sinasc-peso', 'sinasc-apgar1', 'sinasc-apgar5',
                            'sinasc-qtdfilvivo', 'sinasc-qtdfilmort', 'sinasc-gestacao',
                            'sinasc-consultas', 'sinasc-gravidez', 'sinasc-parto',
                            'sinasc-bainasc', 'congenita_diagnosis']]

    df_input_ml['ano'] = df_input_ml['gest-sem_diag'].apply(lambda x: int(str(x)[:-2]))
    df_input_ml = df_input_ml[df_input_ml['ano'].isin(list(range(2012, 2018)))]
    # df_input_ml.drop(columns=['ano'], inplace=True)

    df_input_ml = extra_cleaning(df_input_ml)

    df_input_ml = df_input_ml.fillna(method='ffill')

    # Trocando nome das colunas
    columns_update = columns_name.copy()
    columns_update.extend(['ano'])
    df_input_ml.columns = columns_update

    # Dealing with categorical
    df_input_ml = pd.get_dummies(df_input_ml, columns=categoricos)

    return df_input_ml


def apply_grid_search(X_train, y_train, classifier, parameters):

    # Gridsearch

    #
    # classifier = RandomForestClassifier(random_state=0)

    grid_search = GridSearchCV(estimator=classifier,
                               param_grid=parameters,
                               scoring='roc_auc',
                               cv=10,
                               n_jobs=-1
                               )

    grid_search = grid_search.fit(X_train, np.ravel(y_train))
    print('best_precision: {}'.format(round(grid_search.best_score_, 2)))
    print('best_parameters: {}'.format(grid_search.best_params_))

    return grid_search.best_params_


def apply_cross_val_score(X_train, y_train, classifier):

    accuracies = cross_val_score(estimator=classifier,
                                 X=X_train,
                                 y=y_train,
                                 cv=10
                                 )
    # print(accuracies)
    print('[val_score] mean accuracy: {}'.format(accuracies.mean()))
    print('[val_score] std accuracy: {}'.format(accuracies.std()))


def apply_cross_validate(X_train, y_train, classifier, model_name):

    scoring = {'precision': 'precision',
               'recall': 'recall',
               'f1': 'f1',
               'accuracy': 'accuracy',
               'roc_auc': 'roc_auc'}

    accuracies = cross_validate(estimator=classifier,
                                X=X_train,
                                y=y_train,
                                scoring=scoring,
                                cv=10)

    print('######################### {} ###########################'.format(model_name))
    print('Acurácia: ({}, {})'.format(round(accuracies['test_accuracy'].mean(), 2),
                                      round(accuracies['test_accuracy'].std(), 2)))
    print('Precisão: ({}, {})'.format(round(accuracies['test_precision'].mean(), 2),
                                      round(accuracies['test_precision'].std(), 2)))
    print('Cobertura: ({}, {})'.format(round(accuracies['test_recall'].mean(), 2),
                                       round(accuracies['test_recall'].std(), 2)))
    print('Métrica-F1: ({}, {})'.format(round(accuracies['test_f1'].mean(), 2),
                                        round(accuracies['test_f1'].std(), 2)))
    print('ROC AUC: ({}, {})'.format(round(accuracies['test_roc_auc'].mean(), 2),
                                        round(accuracies['test_roc_auc'].std(), 2)))

    return round(accuracies['test_roc_auc'].mean(), 2)


def show_results(y_test, y_pred):

    report = classification_report(y_test, y_pred, output_dict=True)
    print("Classifier Train Accuracy: {}".format(round(accuracy_score(y_test, y_pred), 2)))
    print("Weight average of precison: {}".format(round(report['macro avg']['precision'], 2)))
    print("Weight average of recall: {}".format(round(report['macro avg']['recall'], 2)))
    print("Weight average of f1-score: {}".format(round(report['macro avg']['f1-score'], 2)))


def show_results_return_df(y_test, y_pred):

    report = classification_report(y_test, y_pred, output_dict=True)

    df = pd.DataFrame([[round(accuracy_score(y_test, y_pred), 2),
                        round(report['macro avg']['precision'], 2),
                        round(report['macro avg']['recall'], 2),
                        round(report['macro avg']['f1-score'], 2)]],
                      columns=['AUC', 'Precision', 'Recall', 'F1-Score'])

    return df


def make_features_importances_df(X_train, classifier, categoricos):

    def return_category(x, categoricos):

        result = ''
        for c in categoricos:
            if re.match('.*' + c + '.*', x):
                return c

        if result == '':
            return x

    df_features = pd.DataFrame()

    df_features['features'] = list(X_train.columns)
    df_features['importances'] = list(classifier.feature_importances_)
    df_features.sort_values(by='importances', inplace=True, ascending=False)

    df_features['category'] = df_features['features'].apply(
        lambda x: return_category(x, categoricos))

    df_features_pivot = pd.pivot_table(df_features, index=['category'],
                                       values=['importances'], aggfunc=np.sum)

    df_features_pivot.sort_values(by=['importances'], ascending=False,
                                  inplace=True)

    df_features_pivot = df_features_pivot.reset_index()

    return df_features_pivot, df_features


def build_confusion_matrix(classifier, X_test, y_test):
    y_pred = classifier.predict(X_test)

    print(confusion_matrix(y_test, y_pred))


def apply_roc_curve(y_test, y_pred):

    false_positive_rate, true_positive_rate, _ = roc_curve(y_test, y_pred)
    roc_auc = auc(false_positive_rate, true_positive_rate)

    plot_roc_curve(false_positive_rate, true_positive_rate, roc_auc)

    return roc_auc


def apply_shap_values(X_train, classifier, m):

    if m in ['random_classifier', 'extratree_classifier',
             'lightGBM_classifier', 'xgboost_classifier']:
        explainer = shap.TreeExplainer(classifier)
        shap_values = explainer.shap_values(X_train, check_additivity=False)
    elif m in ['svm_rbf', 'knn', 'svm_linear']:
        explainer = shap.KernelExplainer(classifier.predict_proba, X_train, link="logit")
        shap_values = explainer.shap_values(X_train, nsamples=5)
    elif m in ['logistic_regression']:
        explainer = shap.LinearExplainer(classifier, X_train, feature_dependence="independent")
        shap_values = explainer.shap_values(X_train)

    if m in ['random_classifier', 'extratree_classifier', 'lightGBM_classifier']:
        df_shap_values_1 = pd.DataFrame(shap_values[1], columns=list(X_train.columns))
    else:
        df_shap_values_1 = pd.DataFrame(shap_values, columns=list(X_train.columns))

    list_importance = list()
    
    for c in list(df_shap_values_1.columns):
        df_shap_values_1[c] = df_shap_values_1[c].apply(lambda x: abs(x))
        list_importance.append(np.mean(df_shap_values_1[c]))
        
    df_feature_importance = pd.DataFrame(columns=["feature", "importance"])
    df_feature_importance["feature"] = list(X_train.columns)
    df_feature_importance["importance"] = list_importance
    df_feature_importance.sort_values(by=["importance"], inplace=True, ascending=False)

    df_feature_importance.to_csv(base_path + "/data_sus/finais/feature_importance_{}.csv".format(m))

    shap_values_1 = shap_values[1]
    shap.summary_plot(shap_values_1, X_train, plot_type="bar")

    return df_feature_importance


def apply_kBest(X_train,  y_train, n):

    return SelectKBest(f_classif, k=n).fit_transform(X_train, y_train)