#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 19:41:25 2020

@author: robson
"""

import pandas as pd
import numpy as np
import os
import re
from iteration_utilities import duplicates

os.chdir('/home/robson/Documents/Mestrado/Topicos avançados em cidades inteligentes/projeto/dados-de-saude')
base_path = os.getcwd()

sifilis_base_sim = ".*A50.*|.*A51.*|.*A52.*|.*A53.*"
sifilis_sinasc = ''

df_sim = pd.read_csv(base_path + '/data_sus/input/sim_ime.csv')
df_sinasc = pd.read_csv(base_path + '/data_sus/input/sinasc_ime.csv')
df_sinasc_sim = pd.read_csv(base_path + '/data_sus/input/sinasc_sim_key.csv')


"""
Sífilis Gestacional

'ID_SINAN_SYPHILIS_GESTATIONAL' - identificação global para sífilis
'ID_SINAN_SYPHILIS' - identificação global para sífilis
'syphilis_diagnosis' - binário (syphilis_diagnosis = 1 if id_syphilis_type == 3 & tpconfirma == 1)
'nu_idade_n' - idade do paciênte em horas, dias, mes e ano (1001 "1 hour" 2001 "1 day" 3001 "1 month" 4001 "1 year" 4100 "100 years" 4101 "101 years" 1000 "0 hours" 2000 "0 days" 3000 "0 months" 4000 "0 years", add)
'cs_gestant' - período gestacional (1 "first trimester" 2 "second trimester" 3 "third trimester" 4 "gestational age ignored" 5 "not pregnant" 6 "not applicable" 9 "ignored")
'tpconfirma' - Teste treponêmico no pré-natal
'cs_raca' - cor de pele da mãe
'cs_escol_n' - código que representa o nível de escolaridade da mãe
'pre_munire' - município da unidade de saúde de realização do pré-natal
'pre_unipre' - unidade de saúde de realização do pré-natal
'tpevidenci' - Classificação clínica da sífilis, 1 - primária, ... 4- latente
'tpteste1' - Refere-se ao resultado do teste não treponêmico para pré-natal (1-Reagente, 2-Não Reagente, 3-Não realizado, 9-Ignorado)
'tpesquema' - Esquema de tratamento preconizado (1-Penicilina G benzatina 2.400.000UI, 1-Penicilina G benzatina 4.800.000UI, 1-Penicilina G benzatina 7.200.000UI, 4-Outro esquema, 5-Não realizado, 9-Ignorado)
'tratparc' - 1 se parceiro foi tratado 0 caso contrário
'tpesqpar' - Esquema de tratamento do parceiro (1-Penicilina G benzatina 2.400.000UI, 1-Penicilina G benzatina 4.800.000UI, 1-Penicilina G benzatina 7.200.000UI, 4-Outro esquema, 5-Não realizado, 9-Ignorado)
'geo_sifil_name_neighborhood' - bairro da unidade de saúde

SINASC

'ID_SINASC' - Identificação Global do sinasc 
'sexo' - sexo do RN
'racacor' - cor de pele do RN
'peso' - peso do RN
'apgar1' - teste apger de 1
'apgar5' - teste apgar de 5
'tprobson' - número da classificação do teste Robson
'idanomal' - binário para identificação de sintomas de congênita
'codanomal' - CDI 10 para anormalidade
'codocupmae' - código de ocupação da mãe
'idadepai' -  idade do pai
'qtdgestant' - número de gestações anteriores
'qtdpartnor' -  quantidade de partos normais realizados
'qtdpartces' -  quantidade de partos cesários realizados
'qtdfilvivo' - quantidade de filhos vivos
'qtdfilmort' - quantidade de abortos
'gravidez' - tipo de gravidez, se único, gêmeos ou trigêmeos
'parto' - binário para tipo de parto, 1 se cesário
'consultas' - quantidade de consultas pré-natal
'consprenat' - número de visitas pré-natal
'gestacao' - quantidade semanas de gestação
'sttrabpart' - binário para indução do trabalho de parto
'stcesparto' - binário para cesário antes da indução do trabalho de parto
'tpapresent' - tipo de apresentação do feto no nascimento
'bainasc' - bairro de nascimento
"""

############################
# Sanitizando a base SIM   #
############################

def sanitiza_sim(df_sim):
    
    df_sim.drop(columns=["r_dtressele", "r_status", "r_explic", "r_compara_cb",
                             "r_nonres", "r_nonresc", "r_cid_caub", "r_icd10b", "adm_numerolote",
                             "adm_retroalim", "adm_dtcadastro", "med_dtatestado", "med_comunsvoim",
                             "med_atestante", "invest_fonte", "invest_dt", "invest_obit"], 
                            inplace=True)
    
    # Concatenando cid
    df_sim['escopo_cid'] = [False] * df_sim.shape[0]
    
    for i in ['cid_causabas', 'cid_causaori']:
        df_sim['escopo_cid'] = df_sim[['escopo_cid',i]].apply(
            lambda x: True if x[0] == True else \
                        True if re.match(sifilis_base_sim, str(x[1])) else \
                        False, axis=1)
    
    
    # Concatenando linhas
    df_sim['escopo_linha'] = [False] * df_sim.shape[0]
    
    for i in ['linhaa', 'linhab', 'linhac', 'linhad',
              'linhaii', 'linhaa_o', 'linhab_o',
              'linhac_o', 'linhad_o', 'linhaii_o']:
    
        df_sim['escopo_linha'] = df_sim[['escopo_linha',i]].apply(
            lambda x: True if x[0] == True else \
                        True if re.match(sifilis_base_sim, str(x[1])) else \
                        False, axis=1)
        
    df_sim.drop(columns=['linhaa', 'linhab', 'linhac', 'linhad',
              'linhaii', 'linhaa_o', 'linhab_o',
              'linhac_o', 'linhad_o', 'linhaii_o'], inplace=True)
    
    # Concatenando cid e linhas
    df_sim['escopo_sifilis'] = df_sim[['escopo_linha', 'escopo_cid']].apply(
                                                                lambda x: True if x[0] == True \
                                                                else True if x[1] == True \
                                                                else False, axis=1)
    
    df_sim = df_sim[df_sim['escopo_sifilis'] == True]
    
    
    # Eliminando colunas com muitos nulos 
    for i in list(df_sim.columns):
        if df_sim[i].isna().sum() > (0.3*df_sim.shape[0]):
            print(i)
            df_sim.drop(columns=[i], inplace=True)
    
    df_sim.to_csv(base_path + '/data_sus/intermediarias/sim_sifilis.csv')


##############################
# Sanitizando a base SINASC  #
##############################
    
def sanitiza_sinasc():
    
    df_sinasc.drop(columns=['horanasc', 'tpmetestim', 
                           'sttrabpart', 'tpnascassi',
                           'fill_tpfuncresp', 'fill_codprof',
                           'fill_orgemissor', 'fill_dtdeclarac',
                           'adm_numerolote', 'adm_dtcadastro'], 
                            inplace=True)
    
    
    #df_sinasc['escopo_linha'] = df_sinasc['codanomal'].apply(
    #    lambda x: True if re.match(sifilis_sinasc, str(x)) else False)
    for i in list(df_sinasc.columns):
        if df_sinasc[i].isna().sum() > (0.3*df_sinasc.shape[0]):
            print(i)
            df_sinasc.drop(columns=[i], inplace=True)
            
    df_sinasc.to_csv(base_path + '/data_sus/intermediarias/sinasc_higienizada.csv')


#############################
# Base Sifilis              #
#############################
    
def sanitiza_sifilis():

    def acquired():
        
        df_simfetal_sinan_acquired = pd.read_csv(
            base_path+'/data_sus/input/simfetal_sinansyphilisacquired_matched_id_rj.csv')
        
        df_sinan_acquired = pd.read_csv(
            base_path+'/data_sus/input/sinan_sifilis_adquirida_ime.csv')
        
        df_sinan_acquired = df_sinan_acquired[
            df_sinan_acquired['syphilis_diagnosis'] == 1]
    
        for i in list(df_sinan_acquired.columns):
            if df_sinan_acquired[i].isna().sum() > (0.3*df_sinan_acquired.shape[0]):
                print(i)
                df_sinan_acquired.drop(columns=[i], inplace=True)
            
        df_sinan_acquired = pd.merge(df_sinan_acquired, df_simfetal_sinan_acquired,
                                     on='ID_SINAN_SYPHILIS_ACQUIRED', how='left')
        
        df_sinan_acquired = pd.merge(df_sinan_acquired, df_sim,
                                     on='ID_SIM', how='left')
        
        df_sinan_acquired.to_csv(
            base_path + '/data_sus/intermediarias/sinan_acquired_sim_matched.csv')
        
    def congenital():
        
        df_sinan_congenita = pd.read_csv(
            base_path + '/data_sus/input/sinan_sifilis_congenita_ime.csv')
        
        df_sinasc_sim_sinan_congenita = pd.read_csv(
             base_path + '/data_sus/input/sinascsim_sinansyphiliscongenital_matched_id_rj.csv')
        
        df_sinan_congenita = df_sinan_congenita[
            df_sinan_congenita['syphilis_diagnosis'] == 1]
        
        for i in list(df_sinan_congenita.columns):
            if df_sinan_congenita[i].isna().sum() > (0.3*df_sinan_congenita.shape[0]):
                print(i)
                df_sinan_congenita.drop(columns=[i], inplace=True)
            
        df_sinan_congenita = pd.merge(df_sinan_congenita,  df_sinasc_sim_sinan_congenita,
                                     on='ID_SINAN_SYPHILIS_CONGENITAL', how='inner')    
        
        df_sinan_congenita = pd.merge(df_sinan_congenita, df_sinasc,
                                     on='ID_SINASC', how='left')
        
        df_sinan_congenita = pd.merge(df_sinan_congenita, df_sim,
                                     on='ID_SIM', how='left')
        
        df_sinan_congenita = df_sinan_congenita.dropna(how='all', axis=1)
        
        df_sinan_congenita.to_csv(
            base_path + '/data_sus/intermediarias/sinan_congenita_matched.csv')
        
    def gestacional():
        
        df_simfetal_sinan_gestacional = pd.read_csv(
        base_path+'/data_sus/input/simfetal_sinansyphilisgestational_matched_id_rj.csv')
        
        df_sinan_gestacional = pd.read_csv(
        base_path+'/data_sus/input/sinan_sifilis_gestante_ime.csv')
        
        df_sinasc_sim_sinan_gestacional = pd.read_csv(
             base_path + '/data_sus/input/sinascsim_sinansyphilisgestational_matched_id_rj.csv',
             sep='|')
        
        df_sinan_gestacional = df_sinan_gestacional[
            df_sinan_gestacional['syphilis_diagnosis'] == 1]
        
        for i in list(df_sinan_gestacional.columns):
            if df_sinan_gestacional[i].isna().sum() > (0.3*df_sinan_gestacional.shape[0]):
                print(i)
                df_sinan_gestacional.drop(columns=[i], inplace=True)
            
        df_sinan_gestacional = pd.merge(df_sinan_gestacional, df_simfetal_sinan_gestacional,
                                     on='ID_SINAN_SYPHILIS_GESTATIONAL', how='left')
        
        df_sinan_gestacional = pd.merge(df_sinan_gestacional, df_sim,
                                     on='ID_SIM', how='left')
        
        df_sinan_gestacional.to_csv(
            base_path + '/data_sus/intermediarias/sinan_gestacional_sim_matched.csv')  

    def link_gestante_rn_congenita():
    
        df_sif_gestacional = df_sinan_gestacional.loc[:,
                                                ['ID_SINAN_SYPHILIS_GESTATIONAL', 
                                                 'ID_SINAN_SYPHILIS',
                                                 'syphilis_diagnosis']]
        
        df_sif_gestacional.columns = ['ID_SINAN_SYPHILIS_GESTATIONAL',
                                      'ID_SINAN_SYPHILIS_gestacional',
                                      'syphilis_diagnosis_gestacional']
        
        df_sif_gestacional = pd.merge(df_sif_gestacional, 
                                      df_sinasc_sim_sinan_gestacional,
                                      on='ID_SINAN_SYPHILIS_GESTATIONAL', 
                                      how='inner')
        
        df_sif_gestacional = pd.merge(df_sif_gestacional, df_sinasc,
                                      on='ID_SINASC', how='left')
        
        df_sif_gestacional.to_csv(
        base_path+'/data_sus/intermediarias/sinan_gestacional_conta_nascidos.csv') 
        
        
        df_sif_gestacional = pd.merge(df_sif_gestacional, 
                                      df_sinasc_sim_sinan_congenita,
                                      on='ID_SINASC', how='left')
        
        '''
        - linkar com a base intermediaria sinasc e congenita
        - linkar com a congenita
        - contar quantas crianças apareceram na congenita e 
        quantas estão somente na base do SINASC
        
        df_sif_gestacional['ID_SINAN_SYPHILIS_CONGENITAL'].nunique()
        Out[49]: 3246
        
        3246/18000
        Out[50]: 0.18033333333333335
        
        '''
        
        df_sif_congenita_gest = df_sinan_congenita.loc[:,
                                                    ['ID_SINAN_SYPHILIS_CONGENITAL', 
                                                     'ID_SINAN_SYPHILIS',
                                                     'syphilis_diagnosis']]
        
        
        df_sif_gestacional = pd.merge(df_sif_gestacional, 
                                      df_sinasc_sim_sinan_gestacional,
                                      on='ID_SINAN_SYPHILIS_GESTATIONAL', 
                                      how='inner')
    
    acquired()
    
    congenital()
    
    gestacional()
    
    df_sif_acquired = pd.read_csv(
    base_path + '/data_sus/intermediarias/sinan_acquired_sim_matched.csv')
    
    df_sif_congenita = pd.read_csv(
    base_path + '/data_sus/intermediarias/sinan_congenita_matched.csv')
    
    df_sif_gestacional = pd.read_csv(
    base_path + '/data_sus/intermediarias/sinan_gestacional_sim_matched.csv')


def sanitiza_populacao():
    '''
        - Primeiro foram pegos os dados do censo e gerado as % dos municipios
        pelas regiões administrativas, de acordo com a aba
        "Pop por bairro 2000-2010"

        - Das estimativas de população, as fontes foram utilizadas da seguinte
        forma:
            - 2000, 2010 Censo e Data Rio
            - 2001 a 2009 e 2011 a 2020 SES/RJ
    '''

    df_pop_censo = pd.read_excel(
        base_path + '/data_sus/input/populacao/Resumo Pop.xlsx',
        sheet_name="Pop por bairro 2000-2010")

    df_pop_censo_ra = pd.pivot_table(df_pop_censo,
                                     index=['Região Administrativa'],
                                     values=['População 2000', 'População 2010'],
                                     aggfunc=np.sum)

    df_pop_censo_ra.columns = ['População 2000_RA', 'População 2010_RA']

    df_pop_censo = pd.merge(df_pop_censo, df_pop_censo_ra,
                            on='Região Administrativa')

    df_pop_censo['% pop 2010'] = df_pop_censo[['População 2010',
                                               'População 2010_RA']].apply(
        lambda x: x[0] / x[1],
        axis=1)

    return df_pop_censo

'''
############################
# Main gera input Sifilis  #
############################
'''


def df_duplic(df, column):

    return df[df[column].isin(list(duplicates(df[column])))].sort_values(column)


def count_duplic(series):

    return len(list(duplicates(series)))


def sanitiza_main_sifilis():

    def build_gestacional():

        df_sinan_gestacional = pd.read_csv(
                base_path+'/data_sus/input/sinan_sifilis_gestante_ime.csv')

        df_sinan_gestacional = df_sinan_gestacional[
                                    df_sinan_gestacional['syphilis_diagnosis'] == 1]

        df_sinan_gestacional = df_sinan_gestacional[
                                                df_sinan_gestacional['female'] == 1]

        # Pegar somente os casos que foram confirmados em que a mãe tem sífilis
        df_sinan_gestacional = df_sinan_gestacional[
                                                df_sinan_gestacional['classi_fin'] == 1]

        df_sinan_gestacional = df_sinan_gestacional[
                                                df_sinan_gestacional['syphilis_diagnosis'] == 1]

        # Higienização da idade, considera somente quem tem acima de 10 anos, entendemos que o restante não refere-se a gestante
        df_sinan_gestacional = df_sinan_gestacional[
                                                df_sinan_gestacional['nu_idade_n'].isin(list(range(4010, 4100)))]

        df_sinan_gestacional['nu_idade_n'] = df_sinan_gestacional['nu_idade_n'].apply(lambda x: int(x) - 4000)

        df_sinan_gestacional = df_sinan_gestacional[['ID_SINAN_SYPHILIS_GESTATIONAL', 'ID_SINAN_SYPHILIS',
               'syphilis_diagnosis', 'nu_idade_n', 'cs_gestant',
               'tpconfirma', 'cs_raca', 'cs_escol_n', 'pre_ufrel', 'pre_munire', 'pre_unipre',
               'tpevidenci', 'tpteste1', 'tpesquema', 'tratparc', 'tpesqpar', 'sem_diag',
               'geo_sifil_name_neighborhood']]

        for i in list(df_sinan_gestacional.columns):
            if df_sinan_gestacional[i].isna().sum() > (0.3*df_sinan_gestacional.shape[0]) and not i == 'pre_unipre':
                df_sinan_gestacional.drop(columns=[i], inplace=True)

        df_sinan_gestacional.columns = ['gest-' + c for c in list(df_sinan_gestacional.columns)]

        df_sinan_gestacional.sort_values(by=['gest-ID_SINAN_SYPHILIS_GESTATIONAL'], inplace=True)

        return df_sinan_gestacional

    def build_sinasc():

        df_sinasc = pd.read_csv(
            base_path+'/data_sus/input/sinasc_ime.csv')

        df_sinasc = df_sinasc[['ID_SINASC', 'sexo', 'racacor', 'peso', 'apgar1', 'apgar5', 'tprobson', 'idanomal',
               'codanomal', 'codocupmae', 'idadepai', 'qtdgestant', 'qtdpartnor', 'qtdpartces', 'qtdfilvivo',
               'qtdfilmort', 'gravidez', 'parto', 'consultas', 'consprenat',
               'gestacao', 'sttrabpart', 'stcesparto', 'tpapresent', 'bainasc']]

        for i in list(df_sinasc.columns):
            if df_sinasc[i].isna().sum() > (0.3*df_sinasc.shape[0]):
                df_sinasc.drop(columns=[i], inplace=True)

        df_sinasc.columns = ['sinasc-' + c for c in list(df_sinasc.columns)]

        return df_sinasc

    def build_congenita():

        df_sinan_congenita = pd.read_csv(
        base_path+'/data_sus/input/sinan_sifilis_congenita_ime.csv')

        df_sinan_congenita = df_sinan_congenita[
            df_sinan_congenita['syphilis_diagnosis'] == 1]

        df_sinan_congenita.columns = ['cong-' + c for c in list(df_sinan_congenita.columns)]

        return df_sinan_congenita

    def read_connection_bases():

        df_sinasc_sinan_gestacional = pd.read_csv(
            base_path+'/data_sus/input/sinascsim_sinansyphilisgestational_matched_id_rj.csv',
            sep='|')

        df_sinasc_sinan_congenita = pd.read_csv(
            base_path+'/data_sus/input/sinascsim_sinansyphiliscongenital_matched_id_rj.csv')

        df_sinasc_sinan_congenita = df_sinasc_sinan_congenita[['ID_SINASC',
                                                               'ID_SINAN_SYPHILIS_CONGENITAL']]


        return df_sinasc_sinan_gestacional, df_sinasc_sinan_congenita

    def merge_bases(df_sinan_gestacional, df_sinasc_sinan_gestacional, df_sinasc, df_sinasc_sinan_congenita):

        df_sinan_gestacional_merge = pd.merge(df_sinan_gestacional,
                                              df_sinasc_sinan_gestacional,
                                              left_on='gest-ID_SINAN_SYPHILIS_GESTATIONAL',
                                              right_on='ID_SINAN_SYPHILIS_GESTATIONAL',
                                              how='left')

        df_sinan_gestacional_merge = pd.merge(df_sinan_gestacional_merge,
                                              df_sinasc,
                                              left_on='ID_SINASC',
                                              right_on='sinasc-ID_SINASC',
                                              how='left')

        df_sinan_gestacional_merge = pd.merge(df_sinan_gestacional_merge,
                                              df_sinasc_sinan_congenita,
                                              on='ID_SINASC',
                                              how='left')

        df_sinan_gestacional_merge['congenita_diagnosis'] = \
            df_sinan_gestacional_merge['ID_SINAN_SYPHILIS_CONGENITAL'].apply(
                lambda x: 1 if x in list(df_sinan_congenita['cong-ID_SINAN_SYPHILIS_CONGENITAL']) else 0)

        return df_sinan_gestacional_merge

    df_sinan_gestacional = build_gestacional()
    df_sinasc = build_sinasc()
    df_sinan_congenita = build_congenita()
    df_sinasc_sinan_gestacional, df_sinasc_sinan_congenita = read_connection_bases()

    df_sinan_gestacional_merge = merge_bases(df_sinan_gestacional, df_sinasc_sinan_gestacional,
                                             df_sinasc, df_sinasc_sinan_congenita)

    df_sinan_gestacional_merge.to_csv(base_path+'/data_sus/finais/sifilis_match_input_ml_v3.csv')

